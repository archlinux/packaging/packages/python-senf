# Maintainer: David Runge <dvzrv@archlinux.org>

_name=senf
pkgname=python-senf
pkgver=1.5.0
pkgrel=7
pkgdesc="Python module for handling platform native strings"
arch=('any')
url="https://github.com/quodlibet/senf"
license=('MIT')
depends=('python')
makedepends=('python-setuptools')
checkdepends=('python-hypothesis' 'python-pytest')
source=("https://files.pythonhosted.org/packages/source/${_name::1}/${_name}/${_name}-${pkgver}.tar.gz"
	"adjust-urlparse-for-python.patch")
sha512sums=('4a474e3bb7ceae07980e27c4d86240b862ec829e7b73fdf0fb43cfd1529cdff7fa7839fa56c354bb37adc950e848612bfd6e90234c8d4a7c143e302361ac7fe4'
            '6eac6dc302b4361a7e019292afd64dfcd3e471cec097b1c89265f35e008d23cf536608382f0193c802be3f1f0164b362d085b41e4b3de904905424d9e2cfafcb')

prepare() {
  mv -v "${_name}-${pkgver}" "${pkgname}-${pkgver}"
  cd "$pkgname-$pkgver"
  # Adjust for urlunparse() repr changes in a recent Python > 3.8
  # That no longer outputs file: as file://.
  # https://github.com/quodlibet/senf/issues/16
  patch -Np1 -i ${srcdir}/adjust-urlparse-for-python.patch
}

build() {
  cd "$pkgname-$pkgver"
  python setup.py build
}

check() {
  cd "$pkgname-$pkgver"
  export PYTHONPATH="build:${PYTHONPATH}"
  # disabling failing tests, that assume a specific user HOME layout:
  # https://github.com/quodlibet/senf/issues/12
  pytest -v -k 'not test_getuserdir and not test_expanduser_user'
}

package() {
  cd "$pkgname-$pkgver"
  python setup.py install --skip-build \
    --optimize=1 \
    --prefix=/usr \
    --root="${pkgdir}"
  install -vDm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -vDm 644 README.rst -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -vDm 644 examples/*.py \
    -t "${pkgdir}/usr/share/doc/${pkgname}/examples"
}
